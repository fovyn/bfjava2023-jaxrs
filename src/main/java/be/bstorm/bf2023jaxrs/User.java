package be.bstorm.bf2023jaxrs;

import lombok.Data;

@Data
public class User {
    private int id;
    private String username;
    private String password;

    private Address address;

}
