package be.bstorm.bf2023jaxrs;

import jakarta.ejb.EJB;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.RequestScoped;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Response;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@Path("/users")
@ApplicationScoped
@Named
public class UserResource {
    @Inject
    private UserRepository repository;

    @GET
    @Consumes("application/json")
    @Produces("application/json")
    public Response getAllAction() {
        return Response
                .ok()
                .entity(repository.findAll())
                .build();
    }

    @GET
    @Path("/{id}")
    @Consumes("application/json")
    @Produces("application/json")
    public Optional<User> getOneAction(@PathParam("id") int id) {
        return repository.findOneById(id);
    }

    @GET
    @Path("/{id}/address")
    @Consumes("application/json")
    @Produces("application/json")
    public Address getAddressByUserIdAction(@PathParam("id") int id) {
        User user = repository.findOneById(id).orElseThrow();

        return user.getAddress();
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response createAction(User user) {
        User created = repository.insert(user);

        return Response
                .created(URI.create("/bf2023-jaxrs-1.0-SNAPSHOT/api/users/"+ created.getId()))
                .entity(created)
                .build();
    }
}
