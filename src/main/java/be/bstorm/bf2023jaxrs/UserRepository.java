package be.bstorm.bf2023jaxrs;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Named;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
@ApplicationScoped
@Named
public class UserRepository {

    static int nbUser = 0;
    static List<User> userList = new ArrayList<>();

    public List<User> findAll() {
        return userList;
    }
    public Optional<User> findOneById(int id) {
        return userList.stream()
                .filter(it -> it.getId() == id)
                .findFirst();
    }
    public User insert(User user) {
        user.setId(++nbUser);
        userList.add(user);

        return user;
    }
}
