package be.bstorm.bf2023jaxrs;

import lombok.Data;

@Data
public class Address {
    private String street;
    private String number;
}
