package be.bstorm.bf2023jaxrs;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/hello-world")
public class HelloResource {
    @GET
//    @Produces("text/plain")
    @Produces(MediaType.APPLICATION_JSON)
    public User hello() {
        User u = new User();
        u.setUsername("Flavian");
        u.setPassword("Le connard/#lesanscoeur");

        return u;
    }
}